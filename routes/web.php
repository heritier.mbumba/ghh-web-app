<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(app()->getLocale());
});


Route::group(['prefix'=>'{locale}', 'where'=>['locale'=>'[a-zA-Z]{2}'], 'middleware'=>'setlocale'], function(){
    
    Route::get('/', 'PageController@index')->name('homepage');
    Route::get('/blogs', 'PageController@blogs')->name('blogs');
    
    Route::get('/music', 'PageController@music')->name('music');
    
    Route::get('/blogs', 'PageController@blogs')->name('blogs');
    
    Route::get('/videos', 'PageController@videos')->name('videos');
    
    Route::get('/about', 'PageController@about')->name('about');
    
    Route::get('/dance', 'PageController@dance')->name('dance');
    
    Route::get('/events', 'PageController@events')->name('events');
    
    Route::get('/countries', 'PageController@countries')->name('countries');
    
    Route::get('/projects', 'PageController@projects')->name('projects');
    
    Route::get('/shop', 'PageController@shop')->name('shop');
    
    //dynamique url
    Route::get('/music/{id}/{title}', 'PageController@getMusicByTitle')->name('getMusicByTitle');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


/**
 * ADMIN ROUTES
 * 
 */
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.showlogin');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login');
Route::post('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::get('/admin/register', 'Auth\AdminLoginController@showRegisterForm')->name('admin.showregister');
Route::post('/admin/register', 'Auth\AdminLoginController@register')->name('admin.register');


Route::group(['prefix'=>'admin', 'middleware'=>'auth:admin', 'as'=>'admin'], function(){
    Route::get('/', 'AdminController@index')->name('.home');
    Route::resource('blog', 'Admin\BlogController');
    Route::resource('club', 'Admin\ClubController');
    Route::resource('client', 'Admin\ClientController')->middleware('role');
    Route::resource('country', 'Admin\CountryController')->middleware('role');
    Route::resource('category', 'Admin\CategoryController');
    Route::resource('group', 'Admin\GroupController');
    Route::resource('event', 'Admin\EventController');
    Route::resource('invoices', 'Admin\InvoiceController')->middleware('role');
    Route::resource('media', 'Admin\MediaController');
    Route::resource('language', 'Admin\LanguageController')->middleware('role');
    Route::resource('music', 'Admin\MusicController');
    Route::resource('orders', 'Admin\OrderController')->middleware('role');
    Route::resource('products', 'Admin\ProductController');
    Route::resource('projects', 'Admin\ProjectController');
    Route::resource('role', 'Admin\RoleController')->middleware('role');
    Route::resource('tag', 'Admin\TagController');
    Route::resource('user', 'Admin\AdminUserController')->middleware('role');
    Route::resource('videos', 'Admin\VideoController');
});


/**
 * CLIENT ROUTES
 */
Route::get('/client/login', 'Auth\ClientLoginController@showLoginForm')->name('client.showlogin');
Route::post('/client/login', 'Auth\ClientLoginController@login')->name('client.login');

Route::get('/client/register', 'Auth\ClientLoginController@showRegisterForm')->name('client.showregister');
Route::post('/client/register', 'Auth\ClientLoginController@register')->name('client.register');

Route::group(['prefix'=>'client', 'middleware'=>'auth:client', 'as'=>'client'], function(){
    Route::get('/', 'Client\ClientController@index')->name('.home');
});




