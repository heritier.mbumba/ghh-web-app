<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = ['language_id', 'name', 'slug', 'flag_url_id', ''];

    public function blogs()
    {
        return $this->hasManyThrough(Blog::class, Admin::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function admin()
    {
        return $this->hasOne(Admin::class);
    }
}
