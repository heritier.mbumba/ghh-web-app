<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    protected $fillable = ['prefix', 'name'];
    
    // public function profile()
    // {
    //     return $this->belongsToMany(Profile::class, 'profile_id');
    // }
}
