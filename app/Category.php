<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ['name', 'slug', 'group_id'];

    public function group(){
        return $this->belongsTo(Group::class);
    }
}
