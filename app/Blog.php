<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable = ['title', 'description', 'slug', 'category_id', 'admin_id', 'country_id'];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
