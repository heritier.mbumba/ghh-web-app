<?php

namespace App\Http\Middleware;

use Closure;

class MustHaveRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(auth()->check() && auth()->user()->role->name == 'administrator'){
            
            return $next($request);
        }
        return redirect()->route('admin.home');
    }
}
