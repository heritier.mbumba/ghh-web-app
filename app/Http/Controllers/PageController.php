<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    public function index()
    {
        return view('index');
    }

    public function music()
    {
        $slides = [
           [
            'url'=>asset('images/music1.jpg'),
            'title'=>'Music',
            'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'
           ],
           ['url'=>asset('images/music2.jpeg'),
           'title'=>'Music',
           'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'], 
           ['url'=>asset('images/music3.jpeg'),
           'title'=>'Music',
           'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'], 
           ['url'=>asset('images/music4.jpg'),
           'title'=>'Music',
           'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.']
        ];
        $title="Music";
        return view('frontend.music', ['title'=>$title, 'slides'=>$slides]);
    }

    public function videos()
    {
        $title="Videos";
        return view('frontend.videos', compact('title'));
    }

    public function blogs()
    {
        $title="Blogs";
        return view('frontend.blogs', compact('title'));

    }

    public function events()
    {
        $title="Event";
        $slides = [
            [
             'url'=>asset('images/event-bg.jpg'),
             'title'=>$title,
             'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'
            ],
            ['url'=>asset('images/event1.jpg'),
            'title'=>$title,
            'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'], 
            ['url'=>asset('images/event2.jpg'),
            'title'=>$title,
            'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.'], 
            ['url'=>asset('images/event3.jpg'),
            'title'=>$title,
            'description'=>'Congealed if none there ne sorrow joyless happy, or lineage in one ah from tales nor old, say come peace.']
         ];
         
         return view('frontend.events', ['title'=>$title, 'slides'=>$slides]);
    }

    public function countries()
    {
        $title="Countries";
        return view('frontend.countries', compact('title'));
    }

    public function about()
    {
        $title="About us";
        return view('frontend.about', compact('title'));
    }
    
    public function projects() {
        $title="Projects";
        return view('frontend.project', compact('title'));
    }
    
    public function dance() {
        $title="Dance";
        return view('frontend.dance', compact('title'));
    }
    
    public function shop() {
        $title="Shop";
        return view('frontend.shop', compact('title'));
    }
}
