<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;
use Auth;
use App\Music;
use App\Club;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //

        return view('admin.country.music.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        return view('admin.country.music.upload', ['categories'=>Category::all(), 'clubs'=>Club::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $this->validate($request, [
            'title'=>'required|min:2',
            'description'=>'required|min:10',
            'artist_name'=>'required|string|min:2',
            'url'=>'required|min:5'
            
        ]);
        $name = 'userid-' . Auth::user()->id . $request->file('feature_image')->getClientOriginalName();
        $path = Storage::putFileAs('public/media', $request->file('feature_image'), $name);

        Music::create(
            [
                'title'=>$request->title, 
                'description'=>$request->description,
                'artist_name'=>$request->artist_name,
                'media_id'=>$request->media_id,
                'admin_id'=>Auth::user()->id,
                'country_Id'=>Auth::user()->country ? Auth::user()->country->name : null,
                'url'=>$request->url,
                'club_id'=>Auth::user
            ]
        );
        //echo asset('storage/file.txt');
        dd(asset($path));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
