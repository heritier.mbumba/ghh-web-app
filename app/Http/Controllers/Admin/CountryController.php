<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Country;
use App\Language;
use App\Admin;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $countries = Country::all();
        return view('admin.country.index', ['countries'=>$countries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.country.register', ['languages'=>Language::all(), 'admins'=>Admin::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'language_id'=>'required|integer',
            'name'=>'required|min:4|string',
        ]);
        Country::create([
            'language_id'=>$request->language_id, 
            'name'=>$request->name, 
            'slug'=>Str::slug($request->name, '-'),
            'admin_id'=>$request->admin_id
            ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //return view('admin.country.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $countries = Country::all();
        $country = Country::where('id', $id)->get()->first();
        
        return view('admin.country.edit', [
            'languages'=>Language::all(), 
            'id'=>$id, 
            'admins'=>Admin::all(), 
            'country'=>$country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $country = Country::find($id);
        $country->language_id = $request->language_id;
        $country->name = $request->name;
        $country->admin_id = $request->admin_id;

        $country->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $country = Country::find($id);
       
        $country->delete();

        return redirect()->back();
    }
}
