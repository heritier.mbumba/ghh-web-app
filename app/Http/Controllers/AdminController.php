<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Auth;
use App\Admin;
use App\Role;

class AdminController extends Controller
{
   
    //
    public function __constructor()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        // if(Auth::user()->role_id == 2){
        //     return redirect()->route('profile.home');
        // }
        return view('admin.index', ['role'=>Auth::user()->role_id]);
        
    }

    public function users()
    {
        $users = Admin::all();
        
        
        return view('admin.user.index', ['users'=>$users]);
    }

    
    

    

    
}
