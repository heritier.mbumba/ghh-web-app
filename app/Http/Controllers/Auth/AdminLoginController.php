<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Admin;
use App\Role;

class AdminLoginController extends Controller
{
    //
    public function __constructor()
    {
        $this->middleware('guest:admin', ['except'=>'logout']);
    }

    public function showLoginForm()
    {

        $exist =  false;
        //check if the super administrator exist
        $check = Admin::where('role_id', 1)->get()->first();
        //does role exist
        $doesRoleExist = Role::all();
        if(sizeof($doesRoleExist) == 0){
            Role::insert([['name'=>'administrator'], ['name'=>'admin']]);
        }
        if($check){
            $exist = true;
        }
        if(Auth::guard('admin')->check())
        {
            
            return redirect()->route('admin.home');
        }
        
        return view('auth.admin-login', ['administratorExist'=>$exist]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember))
        {
            return redirect()->route('admin.home');
        }

        return redirect()->back()->withInput(['email', 'remember']);
    }

    public function showRegisterForm()
    {
        return view('admin.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|min:2',
            'lastname'=>'required|string|min:2',
            'email'=>'required|email',
        ]);
        dd($request->all());
        $doesAdministratorExist = Admin::where('role_id', 1)->get()->first();
        if($doesAdministratorExist == null){
            Admin::create([
                'name'=>$request->name,
                'lastname'=>$request->lastname,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'role_id'=>1
            ]);
            return redirect()->route('admin.showlogin');
        }else{
            dd($request->all());
        }
        
        return true;
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.showlogin');
    }
}
