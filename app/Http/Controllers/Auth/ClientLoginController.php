<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientLoginController extends Controller
{
    //
    public function __constructor()
    {
        $this->middleware('guest:client');
    }

    public function showLoginForm()
    {
        return view('auth.client-login');
    }

    public function login()
    {
        return true;
    }

    public function showRegisterForm()
    {
        return view('client.register');
    }

    public function register(Request $request)
    {
        return true;
    }
}
