<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $fillable = ['name', 'path', 'type', 'admin_id', 'country_id', 'is_admin'];
}
