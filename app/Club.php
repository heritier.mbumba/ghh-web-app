<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    //
    protected $fillable = ['name', 'address', 'slug', 'admin_id', 'country_id'];
}
