$(document).ready(function(){
    $('table').DataTable()
    var password = $('#user-register-form').find('#password')

    passwordGenerator(password)
    
    function passwordGenerator(target)
    {
        var arrayOfLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l']
        var arrayOfNum = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        var arrayOfSymboles = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+']
        var str = ''
        for(var i = 0; i < 3; i++)
        {
            var gen = Math.floor(Math.random() * 12)
            str += arrayOfLetters[gen] + arrayOfNum[gen] + arrayOfSymboles[gen]
        }

        $(target).val(str)
        
        
    }
    
    //CKEDITOR CONFIG
    //CKEDITOR.replace('text-area')
    //CKEDITOR.replace('description')

    window.onload = function() {
        CKEDITOR.replace( 'description' );
    };
    


    var $all = $('[data-image]')
    $all.each(function(i, item){
        var image = $(item).attr('data-image')
        $(item).css({
            background:'url(' + image + ')',
            backgroundPosition:'center',
            backgroundSize:'cover'
        })
        $(item).removeAttr('data-image')
        
    })
    
    
})



