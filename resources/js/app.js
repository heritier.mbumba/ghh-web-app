require('./bootstrap');

$(document).ready( function() {
    $('#myCarousel').carousel({
		interval:   5000,
		pause:false
	});
	
	var clickEvent = false;
	$('#myCarousel').on('click', '.carousel__content_container div', function() {
			clickEvent = true;
			$('.carousel__content_container div').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.carousel__content_container').children().length -1;
			var current = $('.carousel__content_container div.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.carousel__content_container div').first().addClass('active');	
			}
		}
		clickEvent = false;
	});

	$(window).on('scroll', function(){
		if($(this).scrollTop() >= 75){
			$('.header__container').css('background-color', 'rgba(26, 26, 26,.9)')
			$('.logo__container a img').css({
				width:'80px',
			})
			$('.logo__container a').css({
				position:'unset',
			})
			$('.header__container').css({padding:0})
			$('.logo__container').css({
				position:'unset',
				zIndex: 'unset',
				width: 'unset',
				height: 'unset',
			})
		}else{
			$('.header__container').css('background-color', 'unset')
			$('.logo__container a img').css({
				width:'150px',
			})
			$('.logo__container a').css({
				position:'fixed',
			})
			$('.logo__container').css({
				position:'unset',
				zIndex: 'unset',
				width: 'unset',
				height: 'unset',
			})
			$('.header__container').css({padding:'40px'})
		}
		
		
	})

	//add background on header on other page
	var localLangEl = $('.header__container').attr('data-locale')
	
	if(window.location.pathname === localLangEl){
		$('.header__container').css('background-color', '#000')
	}


		//MEGA MENU
		$('#menu-humber').on('click', function(){
			$('.mega__menu_container').css({
				transform:'translateX(0)'
			})

		})
		$('.mega__close').on('click', function(){
			$('.mega__menu_container').css({
				transform:'translateY(-100vw)'
			})
		})
	

	//data-bg-img
	var allBgImg = $('[data-bg-img]')
	$(allBgImg).each(function(i, item){
		var img = $(item).attr('data-bg-img')
		$(this).css({
			background:'url(' + img + ')',
			backgroundPosition:'center',
			backgroundSize:'cover'
		})
		$(this).removeAttr('data-bg-img')
		
	})

	//CAROUSEL
	var owl = $('.owl-carousel').owlCarousel({
		loop:true,
		dots:true,
		items:1,
		margin:10,
		autoplay:true,
		
	});

	owl.on('change.owl.carousel', function(e){
		var el = e.target
		$('.coursel__item_content', el).addClass('slideInUp animated')
										.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
											$('.coursel__item_content').removeClass('slideInUp animated')
										})
	})
	
	
	//Debo script other slide
	
	//Select country 
	$('#selectCountry').mouseover(function(){
		$('.country').css({
			maxHeight: '240px',
			opacity: 1,
		})
	});
	$('#selectCountry').mouseout(function(){
		$('.country').css({
			maxHeight: '0',
			opacity: 0,
		})
	});
	
	//select club select 
	$('#selectClub').mouseover(function(){
		$('.club').css({
			maxHeight: '240px',
			opacity: 1,
		})
	});
	$('#selectClub').mouseout(function(){
		$('.club').css({
			maxHeight: '0',
			opacity: 0,
		})
	});

	//slide up owl carousel content
	var items = $('.owl-item active')

	console.log(items);
	
	
	
	
	
	
	
	
	
});
