<form method="POST" action="{{ route('admin.register') }}" class="admin__login_form">
    @csrf
  <h1>Register administrator</h1>
  <div class="form-group">
    <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="First name" required name="name" value="{{ old('name') }}" autocomplete="name" autofocus/>
    @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
  </div>
  <div class="form-group">
    <input type="text" class="form-control @error('lastname') is-invalid @enderror" placeholder="Last name" required name="lastname" value="{{ old('lastname') }}" autocomplete="lastname" autofocus/>
    @error('lastname')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
  </div>
  <div class="form-group">
    <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" required name="email" value="{{ old('email') }}" autocomplete="email" autofocus/>
    @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
  </div>
  <div>
    <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required name="password" required autocomplete="current-password"/>
    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
  </div>
  
  <div>
    <button type="submit" class="btn btn-primary">
      Register
    </button>
  </div>
  <div class="clearfix"></div>

  <div class="separator">
    <div>
      <h4><i class="fa fa-paw"></i> Generation Hip Hop Global</h4>
      <p>© <span id="footer_date"></span> All Rights Reserved. Generation Hip Hop Global</p>
    </div>
  </div>
</form>