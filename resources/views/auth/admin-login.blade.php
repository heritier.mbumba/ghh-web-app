<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login | Generation Hip Hop Global</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{asset('css/admin-login.css')}}">
  </head>

  <body class="login d-flex align-items-center justtify-content-center">
    <div class="container">
      <div class="row">
        <div class="col-md-4 offset-md-4">
          
          @if ($administratorExist)
          <form method="POST" action="{{ route('admin.login') }}" class="admin__login_form">
            @csrf
          <h1>Login Form</h1>
          <div>
            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" required name="email" value="{{ old('email') }}" autocomplete="email" autofocus/>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div>
            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required name="password" required autocomplete="current-password"/>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="form-group row">
            <div class="col-md-6 offset-md-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>
          <div>
            <button type="submit" class="btn btn-primary">
              Login
            </button>
            @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
          </div>
          <div class="clearfix"></div>

          <div class="separator">
            <div>
              <h4><i class="fa fa-paw"></i> Generation Hip Hop Global</h4>
              <p>© <span id="footer_date"></span> All Rights Reserved. Generation Hip Hop Global</p>
            </div>
          </div>
        </form>
        @else 
            <p class="text-center">There is no administrator account please <a href="{{ route('admin.register') }}" class="text-center">create one</a></p>
            
        @endif
          
        </div>
      </div>
    </div>

    <script>
        var el = document.getElementById('footer_date')
        var currentYear = new Date().getFullYear()
        el.innerText = currentYear
    </script>
  </body>
</html>
