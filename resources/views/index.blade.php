@extends('layouts.frontend')

@section('content')
   @include('includes.banner-slider')

   <div class="home__page_content">
      @component('components.section')
         <div class="col-md-4 pl-0">
            <div class="component_content">
               <h2>{{__('upcoming events')}}</h2>
               <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente temporibus iusto deserunt.</p>
               <a href="" class="btn btn-arrow pl-0">Browser more <span class="icon-right-big"></span></a>
            </div>
         </div>
         <div class="col-md-8">
            <div class="component__item"></div>
            <div class="component__item"></div>
            <div class="component__item"></div>
         </div>
      @endcomponent

      <section class="boxcontent__container">
         <div class="container">
            <div class="row">
                  <div class="col-md-3">
                     <div class="boxcontent__item" data-bg-img="{{ asset('images/music-bg.jpg') }}">
                        <div class="boxcontent__content">
                            <h3>Music</h3>
                            <div class="boxcontent__content_description">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                                <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">find out more</a>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                     <div class="boxcontent__item" data-bg-img="{{ asset('images/dance-bg.jpg') }}">
                        
                        <div class="boxcontent__content">
                            <h3>Dance</h3>
                            <div class="boxcontent__content_description">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                                <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">find out more</a>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                     <div class="boxcontent__item" data-bg-img="{{ asset('images/event-bg.jpg') }}">
                        
                        <div class="boxcontent__content">
                            <h3>Upcoming Event</h3>
                            <div class="boxcontent__content_description">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                                <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">find out more</a>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                     <div class="boxcontent__item" data-bg-img="{{ asset('images/shop-bg.jpg') }}">
                        
                        <div class="boxcontent__content">
                            <h3>Shop</h3>
                            <div class="boxcontent__content_description">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                                <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">find out more</a>
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
         </div>
      </section>

      @component('components.section')
         <div class="col-md-8">
            <div class="component__item"></div>
            <div class="component__item"></div>
            <div class="component__item"></div>
         </div>
            <div class="col-md-4 pr-0">
               <div class="component_content">
               <h2>{{__('dance projects')}}</h2>
               <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente temporibus iusto deserunt.</p>
               <a href="" class="btn btn-arrow pl-0">Browser more <span class="icon-right-big"></span></a>
            </div>
            </div>
         
      @endcomponent

      <section class="feature__products pt-5">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <h2>Our feature products</h2>
               </div>
            </div>
            <div class="row" id="feature-products">
               <div class="col-md-3">
                  <div class="feature__item">
                     <div class="image__holder">
                        <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                     </div>
                     <div class="description">
                        <h4>Track 1</h4>
                        <span>artist name</span>
                     </div>
                     <div class="action">
                        <button type="button" class="btn btn-secondary" id="list"><span class="icon-play"></span> Listen</button>
                        <button type="button" class="btn btn-primary" id="add"><span class="icon-cart-plus"></span> Add to cart</button>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="feature__item">
                     <div class="image__holder">
                        <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                     </div>
                     <div class="description">
                        <h4>Track 2</h4>
                        <span>artist name</span>
                     </div>
                     <div class="action">
                        <button type="button" class="btn btn-secondary" id="list"><span class="icon-play"></span> Listen</button>
                        <button type="button" class="btn btn-primary" id="add"><span class="icon-cart-plus"></span> Add to cart</button>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="feature__item">
                     <div class="image__holder">
                        <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                     </div>
                     <div class="description">
                        <h4>Track 3</h4>
                        <span>artist name</span>
                     </div>
                     <div class="action">
                        <button type="button" class="btn btn-secondary" id="list"><span class="icon-play"></span> Listen</button>
                        <button type="button" class="btn btn-primary" id="add"><span class="icon-cart-plus"></span> Add to cart</button>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="feature__item">
                     <div class="image__holder">
                        <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                     </div>
                     <div class="description">
                        <h4>Track 4</h4>
                        <span>artist name</span>
                     </div>
                     <div class="action">
                        <button type="button" class="btn btn-secondary" id="list"><span class="icon-play"></span> Listen</button>
                        <button type="button" class="btn btn-primary" id="add"><span class="icon-cart-plus"></span> Add to cart</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      @component('components.section')
         <div class="col-md-4 pl-0">
            <div class="component_content">
               <h2 class="display-2">{{__('new music track')}}</h2>
               <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente temporibus iusto deserunt.</p>
               <a href="" class="btn btn-arrow pl-0">Browser more <span class="icon-right-big"></span></a>   
            </div>
         </div>
         <div class="col-md-8">
            <div class="component__item"></div>
            <div class="component__item"></div>
            <div class="component__item"></div>
         </div>
      @endcomponent

      <section class="feature-activities pt-5 pb-5">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="activities__item">
                     <div class="activities__image">
                        <img src="{{ asset('images/video-placeholder.png') }}" alt="">
                     </div>
                     <div class="activities__content">
                        <h4>activity name</h4>
                        <span>country name</span>
                     </div>
                     <a href="" class="btn btn-primary">read more</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="activities__item">
                     <div class="activities__image">
                        <img src="{{ asset('images/video-placeholder.png') }}" alt="">
                     </div>
                     <div class="activities__content">
                        <h4>activity name</h4>
                        <span>country name</span>
                     </div>
                     <a href="" class="btn btn-primary">read more</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="activities__item">
                     <div class="activities__image">
                        <img src="{{ asset('images/video-placeholder.png') }}" alt="">
                     </div>
                     <div class="activities__content">
                        <h4>activity name</h4>
                        <span>country name</span>
                     </div>
                     <a href="" class="btn btn-primary">read more</a>
                  </div>
               </div>
            </div>
         </div>
      </section>

      @component('components.section')
         <div class="col-md-8">
            <div class="component__item"></div>
            <div class="component__item"></div>
            <div class="component__item"></div>
         </div>
         <div class="col-md-4 pr-0">
            <div class="component_content">
               <h2 class="display-2">{{__('videos and tv show')}}</h2>
               <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente temporibus iusto deserunt.</p>
               <a href="" class="btn btn-arrow pl-0">Browser more <span class="icon-right-big"></span></a>   
            </div>
         </div>
      @endcomponent

      <section class="latest-club-event pt-5 pb-5">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="club_content">
                     <img src="{{ asset('images/item-shop1.jpg') }}" alt="">
                     <div class="club_description">
                        <h4>AWESOME MUSIC ALBUM</h4>
                        <span>$49.90</span>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente quidem quam fugiat ratione dolorem tempore?</p>
                        <button class="btn btn-primary">Add to cart</button>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="club_content">
                     <img src="{{ asset('images/item-shop4.jpg') }}" alt="">
                     <div class="club_description">
                        <h4>Awesome dance</h4>
                        
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente quidem quam fugiat ratione dolorem tempore?</p>
                       
                     </div>
                  </div>
                  <div class="club_content">
                     <img src="{{ asset('images/item-shop3.jpg') }}" alt="">
                     <div class="club_description">
                        <h4>Awesome dance</h4>
                        
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente quidem quam fugiat ratione dolorem tempore?</p>
                        
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="club_content">
                     <img src="{{ asset('images/item-shop2.jpg') }}" alt="">
                     <div class="club_description">
                        <h4>Awesome dance</h4>
                        <span>$49.90</span>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente quidem quam fugiat ratione dolorem tempore?</p>
                        <button class="btn btn-primary">Add to cart</button>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
    
@endsection