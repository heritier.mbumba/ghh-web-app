<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="qki-container__music-events--content__box-media u-margin-bottom-big">
                    <h1>View More Generation Hip Hop Music on: </h1>
                    
                    <ul class="qki-container__music-events--content__box-media--nav">
                        <li class="qki-container__music-events--content__box-media--nav__item">
                            <a href="#" class="qki-container__music-events--content__box-media--nav__link"><i class="icon-facebook-5"></i></a>
                        </li>
                        <li class="qki-container__music-events--content__box-media--nav__item">
                            <a href="#" class="qki-container__music-events--content__box-media--nav__link"><i class="icon-twitter-5"></i></a>
                        </li>
                        <li class="qki-container__music-events--content__box-media--nav__item">
                            <a href="#" class="qki-container__music-events--content__box-media--nav__link"><i class="icon-instagram"></i></a>
                        </li>
                        <li class="qki-container__music-events--content__box-media--nav__item">
                            <a href="#" class="qki-container__music-events--content__box-media--nav__link"><i class="icon-linkedin-5"></i></a>
                        </li>
                        <li class="qki-container__music-events--content__box-media--nav__item">
                            <a href="#" class="qki-container__music-events--content__box-media--nav__link"><i class="icon-pinterest"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>