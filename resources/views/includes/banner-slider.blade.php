<div class="container-fluid">
  <div class="row">
    <div class="banner__slider__container">
      <div class="banner__slider">
        <div class="owl-carousel">
          <div class="coursel__item d-flex align-items-end" data-bg-img="{{asset('images/slide1.jpg')}}">
            <div class="container coursel__item_content">
              <h2>We're proud to have GHH Chapters worldwide!</h2>
              <p>Lorem ipsum dolor sit amet.</p>
              <a href="" class="btn btn-primary">read more</a>
            </div>
          </div>
          <div class="coursel__item d-flex align-items-end" data-bg-img="{{asset('images/slide2.jpg')}}">
            <div class="container coursel__item_content">
              <h2>We are in 61 countries and growing</h2>
              <p>Lorem ipsum dolor sit amet.</p>
              <a href="" class="btn btn-primary">read more</a>
            </div>
          </div>
          <div class="coursel__item d-flex align-items-end" data-bg-img="{{asset('images/slide3.jpg')}}">
            <div class="container coursel__item_content">
              <h2>Check out these hip hop videos from across the globe!</h2>
              <p>Lorem ipsum dolor sit amet.</p>
              <a href="" class="btn btn-primary">read more</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>