<div id="mySlide" class="carousel slide" data-ride="carousel">
    {{-- carousel items START --}}
    <div class="carousel-inner music-slide-inner">

      <div class="carousel-item active">
        <img class="d-block w-100" src="{{ asset('images/event1.jpg') }}" alt="First slide">
        <div class="carousel-caption">
            <h2 class="display-2">Category Event's name</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
            <p><span>20 - 23 March 2020</span></p>
            {{-- <button type="submit"></button> --}}
            <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">View Event Info</a>
          </div>
      </div>

      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('images/event2.jpg') }}" alt="Second slide">
        <div class="carousel-caption">
            <h2 class="display-2">Category Event's name</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
            <p><span>23 June 2020</span></p>
            <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">View Event Info</a>
          </div>
      </div>

      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('images/event3.jpg') }}" alt="Third slide">
        <div class="carousel-caption">
            <h2 class="display-2">Category Event's name</h2>
            <p class="">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
            <p><span>20 June - 23 July 2020</span></p>
            <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">View Event Info</a>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('images/event-bg.jpg') }}" alt="Third slide">
        <div class="carousel-caption">
            <h2 class="display-2">Category Event's name</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
            <p><span>20 - 23 December 2020</span></p>
            <a href="" class="btn btn-arrow-s pl-0 pt-0 pb-0">View Event Info</a>
        </div>
      </div>

    </div>
    {{-- carousel items END --}}
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slide__content_container d-flex align-items-center justify-content-between ui-margin-top">
                    <div data-target="#mySlide" data-slide-to="0" class="active"">
                        <div class="slide__content">
                            <p class="slide__content--paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                        </div>
                    </div>
                    <div data-target="#mySlide" data-slide-to="1" ">
                        <div class="slide__content">
                            <p class="slide__content--paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                        </div>
                    </div>
                    <div data-target="#mySlide" data-slide-to="2" ">
                        <div class="slide__content">
                            <p class="slide__content--paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                        </div>
                    </div>
                    <div data-target="#mySlide" data-slide-to="3" ">
                        <div class="slide__content">
                            <p class="slide__content--paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, consequuntur!</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

  </div>