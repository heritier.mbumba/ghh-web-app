<div class="mega__menu_container">
    <div class="mega__close"><span class="icon-cancel-circle-2"></span></div>
    <div class="mega__menu_content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <ul class="main__menu">
                        <li><a href="{{route('countries', app()->getLocale())}}">Country</a></li>
                        <li><a href="{{route('events', app()->getLocale())}}">Events</a></li>
                        <li><a href="{{route('music', app()->getLocale())}}">Music</a></li>
                        <li><a href="{{route('dance', app()->getLocale())}}">Dance</a></li>
                        <li><a href="{{route('videos', app()->getLocale())}}">Video</a></li>
                        <li><a href="{{route('blogs', app()->getLocale())}}">Blogs</a></li>
                        <li><a href="{{route('projects', app()->getLocale())}}">Projects</a></li>
                        <li><a href="{{route('shop', app()->getLocale())}}">Shop</a></li>
                    </ul>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-3">
                    <div class="mega__side_content">
                        <h4>Featured content</h4>
                        <div class="feature_content">
                            <div class="feature_item d-flex align-items-start">
                                <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                                <div class="feature_item_content">
                                    <h4>Club danse 1</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur...</p>
                                    <a href="">read more <span class="icon-right-open"></span></a>
                                </div>
                            </div>
                            <div class="feature_item d-flex align-items-start">
                                <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                                <div class="feature_item_content">
                                    <h4>Music new 1</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur...</p>
                                    <a href="">read more <span class="icon-right-open"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>