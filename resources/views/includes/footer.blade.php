<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav">
                    <li class="nav__item"><a href="#" class="nav__link">PRIVACY POLICY</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">TERMS</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">COUNTRIES</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">CONTACT US</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">SHOP</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">MEDIA</a></li>
                </ul>
                <p class="copyright">
                    &copy; 2020 Copyright Generation Hip Hop - GLOBAL.
                </p>
            </div>
        </div>
    </div>
</footer>
