<header class="header__container" data-locale="{{ app()->getLocale() }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="logo__container">
                    <a href="{{ route('homepage', app()->getLocale()) }}"><img src="{{ asset('images/ghh-logo-white.png') }}" alt="ghh logo"></a>
                </div>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-3">
                <div class="header__menu_controller d-flex align-items-center pr-2">
                    <div class="header__menu_item" id="search">
                        <span class="icon-search-7"></span>
                    </div>
                    <div class="header__menu_item" id="account">
                        <span class="icon-user-2"></span>
                    </div>
                    <div class="header__menu_item" id="menu-humber">
                        <span class="icon-menu-3"></span>
                    </div>
                    <div class="header__menu_item" id="lang">
                        <span class="icon-globe-3"></span> <span class="lang">{{ app()->getLocale() }}</span> <span class="arrow icon-down-dir"></span>
                        <ul class="lang__list">
                            @foreach (config('app.available_locales') as $langKey=>$lang)
                                <li><a href="/{{ $langKey }}"><span class="lang">{{ $langKey }}</span> : {{  $lang }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
