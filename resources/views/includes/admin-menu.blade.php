<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
      <div class="navbar nav_title" style="border: 0;">
        <a href="/admin" class="site_title"><span>Generation Hip Hop Global</span></a>
      </div>

      <div class="clearfix"></div>

      <!-- menu profile quick info -->
      <div class="profile clearfix">
        <div class="profile_pic">
          <img src="{{asset('images/img.jpg')}}" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
          <span>Welcome,</span>
        <h2>{{ Auth::user()->name }} {{ Auth::user()->lastname }} <br /><small>{{ Auth::user()->role->name }}</small></h2>
        </div>
      </div>
      <!-- /menu profile quick info -->

      <br />

      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <h3>
            @if (Auth::user()->role->name === 'admin' && Auth::user()->country)
            {{Auth::user()->country->name}}
            @endif
          </h3>
          <ul class="nav side-menu">
            <li><a><i class="icon-th-thumb-empty"></i> {{Auth::user()->role->name == 'administrator' ? 'Features' : 'Overview'}} <span class="icon-down-dir"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ route('adminmusic.index') }}">Music</a></li>
                <li><a href="{{ route('adminvideos.index') }}">Videos</a></li>
                <li><a href="{{ route('adminevent.index') }}">Events</a></li>
              </ul>
            </li>
            <li><a><i class="icon-globe-3"></i> {{Auth::user()->role->name == 'administrator' ? 'Countries' : 'Features'}} <span class="icon-down-dir"></span></a>
              <ul class="nav child_menu">
                @if (Auth::user()->role->name == 'administrator')
                <li><a href="{{ route('admincountry.index') }}">All</a></li>
                <li><a href="{{ route('admincountry.create') }}">Add</a></li>
                @endif
                <li><a href="{{ route('adminclub.index') }}">Clubs</a></li>
                <li><a href="{{ route('adminprojects.index') }}">Projects</a></li>
                <li><a href="{{ route('adminblog.index') }}">Blogs</a></li>
                
              </ul>
            </li>
            <li><a><i class="icon-shop"></i> Products <span class="icon-down-dir"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ route('adminproducts.index') }}">All</a></li>
                <li><a href="{{ route('adminproducts.create') }}">Add</a></li>
                @if (Auth::user()->role->name == 'administrator')
                <li><a href="{{ route('adminorders.index') }}">Orders</a></li>
                <li><a href="{{ route('admininvoices.index') }}">Invoices</a></li>
                <li><a href="{{ route('adminclient.index') }}">Clients</a></li>
                @endif
               
              </ul>
            </li>
          </ul>
        </div>
        <div class="menu_section">
          <h3>Extra</h3>
          <ul class="nav side-menu">
            @if (Auth::user()->role->name == 'administrator')
            <li><a><i class="icon-users"></i> Users <span class="icon-down-dir"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ route('adminuser.index') }}">All</a></li>
                <li><a href="{{ route('adminuser.create') }}">Add</a></li> 
                <li><a href="{{ route('adminrole.index') }}">Roles</a></li>
                
                
              </ul>
            </li>
            @endif
            <li><a><i class="icon-file-image"></i> Media <span class="icon-down-dir"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('adminmedia.index') }}">all</a></li>
                    <li><a href="{{ route('adminmedia.create') }}">Upload<span class="icon-down-dir"></span></a></li>
                    <li><a href="#level1_2">Trash</a></li>
                </ul>
            </li>       
            <li><a><i class="icon-cog-alt"></i> Utilities <span class="icon-down-dir"></span></a>
              <ul class="nav child_menu">
                @if (Auth::user()->role->name == 'administrator')
                <li><a href="{{ route('adminlanguage.index') }}">Languages</a></li>
                @endif
                <li><a href="{{ route('admincategory.index') }}">Categories</a></li>
                <li><a href="{{ route('admingroup.index') }}">Categories groups</a></li>
              </ul>
            </li>
                       
            
          </ul>
        </div>

      </div>
      <!-- /sidebar menu -->

      <!-- /menu footer buttons -->
      <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
          <span class="icon-cog-1" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Dashboard">
          <span class="icon-home-outline" aria-hidden="true"></span>
        </a>
        <a data-placement="top" title="Website" href="{{ route('homepage', app()->getLocale()) }}" target="_blank">
          <span class="icon-globe"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="#" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
          <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </div>
      <!-- /menu footer buttons -->
    </div>
  </div>