<footer class="footer__container">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="widget__container">
                    <h4 class="widget__title">About us</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, commodi explicabo aperiam soluta quas sed.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget__container">
                    <h4 class="widget__title">Contact & Address</h4>
                    <p><i class="icon-phone"></i> Tel 877-594-1292</p>
                    <p><i class="icon-email"></i> info@generationhiphopglobal.org</p>
                    <p><i class="icon-address"></i> 9 12th Ave, Houghton Estate, Johannesburg, South Africa 2198   </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget__container">
                    <h4 class="widget__title">Social media</h4>
                    <ul class="d-flex align-item-center">
                        <li><a href=""><i class="icon-facebook-5"></i></a></li>
                        <li><a href=""><i class="icon-twitter-5"></i></a></li>
                        <li><a href=""><i class="icon-instagram"></i></a></li>
                        <li><a href=""><i class="icon-linkedin-5"></i></a></li>
                        <li><a href=""><i class="icon-linkedin-5"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__credit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="foot__content">&copy; <span id="footer_date"></span> All right reseved | Website developed by <a href="http://quirkyinnovations.co.za/" target="_blank">Quirky innovations</a></div>
                </div>
            </div>
        </div>
    </div>
</footer>
