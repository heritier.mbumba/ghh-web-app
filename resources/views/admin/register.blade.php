<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login | Generation Hip Hop Global</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{asset('css/admin-login.css')}}">
  </head>

  <body class="login d-flex align-items-center justtify-content-center">
    <div class="container">
      <div class="row">
        <div class="col-md-4 offset-md-4">
          
            <form method="POST" action="{{ route('admin.register') }}" class="admin__login_form">
                @csrf
              <h1>Register administrator</h1>
              <div class="form-group">
                <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="First name" required name="name" value="{{ old('name') }}" autocomplete="name" autofocus/>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="text" class="form-control @error('lastname') is-invalid @enderror" placeholder="Last name" required name="lastname" value="{{ old('lastname') }}" autocomplete="lastname" autofocus/>
                @error('lastname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" required name="email" value="{{ old('email') }}" autocomplete="email" autofocus/>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <input type="telephone" class="form-control @error('phone') is-invalid @enderror" placeholder="Cellphone" required name="phone" value="{{ old('phone') }}" autocomplete="phone" autofocus/>
                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div>
                <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required name="password" required />
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              
              <div>
                <button type="submit" class="btn btn-primary mb-2">
                  Register
                </button>
              </div>
              <div class="clearfix"></div>
            
              <div class="separator">
                <div>
                  <h4><i class="fa fa-paw"></i> Generation Hip Hop Global</h4>
                  <p>© <span id="footer_date"></span> All Rights Reserved. Generation Hip Hop Global</p>
                </div>
              </div>
            </form>
        
        </div>
      </div>
    </div>

    <script>
        var el = document.getElementById('footer_date')
        var currentYear = new Date().getFullYear()
        el.innerText = currentYear
    </script>
  </body>
</html>
