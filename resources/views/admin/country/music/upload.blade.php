@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Upload</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('adminmusic.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-9">
                        <div class="x_panel">
                            <div class="form-group">
                                <label for="">Music title</label>
                                <input type="text" name="title" id="" class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <label for="">Music description</label>
                                <textarea name="description" id="" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Url</label>
                                <input type="text" name="url" id="" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="media_id" id="">
                            </div>
                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Options</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <label for="">Category</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="">select...</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Club</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="">select...</option>
                                        @foreach ($clubs as $club)
                                            <option value="{{ $club->id }}">{{ $club->name }}</option>
                                        @endforeach
                                    </select>
                                    <a href="{{route('adminclub.create')}}" class="btn pl-0">Add new club</a>
                                </div>
                                @if (Auth::user()->role->name == 'administrator')
                                    <a href="{{ route('admincategory.index') }}">add category</a>
                                @endif
                                <hr />
                                <div class="form-group">
                                    <label for="">Tags</label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                                <a href="" class="btn pl-0 text-danger">Delete</a>
                            </div>
                        </div>
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Options</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <label for="">Artist name</label>
                                    <input type="text" name="artist_name" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Album image</label>
                                    <input type="file" name="feature_image" id="">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection