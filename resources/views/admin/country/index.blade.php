@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table id="datatable" class="table table-striped " style="width:100%">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Managed By</th>
                    <th>Language</th>
                    <th>Actions</th>
                    
                  </tr>
                </thead>
                <tbody>

                  @foreach ($countries as $country)
                      <tr>
                        <td><a href="{{ route('admincountry.edit', $country->id) }}">{{$country->name}}</a></td>
                        <td class="d-flex align-items-center">
                          <span class="icon-user-o"></span>
                          <span>{{isset($country->admin->name) ? $country->admin->name : 'N/A'}}</span> 
                          
                        </td>
                        <td>{{isset($country->language->name) ? $country->language->name : 'N/A'}}</td>
                        <td class="d-flex align-item-center">
                          <a href="{{ route('admincountry.edit', $country->id) }}" class="btn pb-0 pt-0"><span class="icon-edit"></span></a>
                          
                          <form action="{{ route('admincountry.destroy', $country->id) }}" method="post" id="delete-country">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class=""><span class="icon-trash-empty"></span></button>
                          </form>
                        </td>
                      </tr>
                  @endforeach
                    
                </tbody>
              </table>
        </div>
    </div>
@endsection