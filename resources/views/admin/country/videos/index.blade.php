@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>videos</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <a href="{{ route('adminvideos.create') }}" class="btn btn-success">upload video</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Preview</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection