@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Upload</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-9">
                        <div class="x_panel">
                            <div class="form-group">
                                <label for="">Video title</label>
                                <input type="text" name="title" id="" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="file" name="" id="">
                            </div>
                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Categories</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <hr />
                                <div class="form-group">
                                    <label for="">Tags</label>
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                                <a href="" class="btn pl-0 text-danger">Delete</a>
                            </div>
                        </div>
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Options</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <label for="">Feature image</label>
                                    <input type="file" name="" id="">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection