@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Create new project</h2>
                        <div class="clearfix"></div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('adminclub.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="form-group">
                                        <label for="">Title</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <p></p>
                                        <button type="submit" class="btn btn-success">submit</button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Categories</h2>
                                    <div class="clearfix"></div>
                                    <div class="x_content">
                                        <p></p>
                                        <div class="panel_category">
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                            <span class="d-block"><input type="checkbox"> <label for="">Feature image</label></span>
                                        </div>
                                        <p class="mt-4">
                                            <a href="#" class="text-sm-left" id="add-new-category">Add new category</a>
                                        </p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Options</h2>
                                    <div class="clearfix"></div>
                                    <div class="x_content">
                                        <p></p>
                                        <div class="form-group">
                                            <label for="">Status</label>
                                            <select name="status" id="" class="form-control">
                                                <option value="">Publish</option>
                                                <option value="">Draft</option>
                                                <option value="">unplanish</option>
                                            </select>
                                        </div>
                                        <p></p>
                                        <div class="form-group">
                                            <label for="">Feature image</label>
                                            <input type="file">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tags</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <p></p>
                                            <button type="submit" class="btn pl-0 text-danger">delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
    
@endsection