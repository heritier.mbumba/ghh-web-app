@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Register new country<small>profile</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br />
                <form action="{{ route('admincountry.store') }}" method="post" class="row form-horizontal form-label-left">
                    @csrf
                    
                    <div class="col-md-10">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Language Spoken <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select name="language_id" class="form-control" required>
                                    <option value="">Choose..</option>
                                    @foreach ($languages as $lang)
                                        <option value="{{$lang->id}}">{{$lang->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="name" name="name" required="required" class="form-control">
                            </div>
                            </div>
                            <div class="item form-group">
                            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Assign Admin</label>
                            <div class="col-md-6 col-sm-6 ">
                                <select name="admin_id" class="form-control">
                                    <option value="">Choose..</option>
                                    @foreach ($admins as $admin)
                                        <option value="{{$admin->id}}">{{$admin->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                            <div class="col-md-6 col-sm-6 offset-md-3">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button type="button" class="btn btn-success">cancel</button>
                            </div>
                            </div>        
                    </div>
                </form>
                </div>
            </div>
        </div>
  </div>
    
@endsection