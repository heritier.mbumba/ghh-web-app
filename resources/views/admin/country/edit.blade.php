@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="d-flex justify-content-between align-items-center">
                        <h3>{{$country->name}}</h3>
                        <div class="actions">
                            <a href="{{route('adminclub.create')}}" class="mr-2"><span class="icon-plus-1"></span>Add clubs</a>
                            <a href="{{route('adminprojects.create')}}" class="mr-2"><span class="icon-briefcase-3"></span>Add projects</a>
                            <a href="{{route('adminproducts.create')}}" class="mr-2"><span class="icon-cart-plus"></span>Add products</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="x_panel">
                            <div class="x_content text-center">
                                <h2>4</h2>
                                <span>Blogs</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="x_panel">
                            <div class="x_content text-center">
                                <h2>5</h2>
                                <span>Projects</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="x_panel">
                            <div class="x_content text-center">
                                <h2>6</h2>
                                <span>Products</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="x_panel">
                            <div class="x_content text-center">
                                <h2>6</h2>
                                <span>Clubs</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recent activities</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="activity_item d-flex justify-content-between">
                        <div class="description">
                            <i>Recent activity</i>
                        </div>
                        <div class="activity_item_action d-flex align-items-center">
                            <a href="" class="btn"><span class="icon-eye"></span></a>
                            <form action="" method="post">
                                <button type="submit"><span class="icon-trash-empty"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="col-md-3">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Edit</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="">Country name</label>
                            <input type="text" name="name" id="" value="{{$country->name}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Spoken language</label>
                            <select name="language_id" id="" class="form-control">
                                @foreach ($languages as $lang)
                                    <option value="">{{$lang->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Managed by</label>
                            @if ($country->admin)
                                <span>{{ $country->admin->name }}</span>
                            @else
                                <span>N/A</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection