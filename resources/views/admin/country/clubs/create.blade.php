@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Create new club</h2>
                        <div class="clearfix"></div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('adminclub.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="form-group">
                                        <label for="">Club Name</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        @error('name')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Address</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="address">
                                        </div>
                                    </div>
                                    <p class="pt-3"></p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Options</h2>
                                    <div class="clearfix"></div>
                                    <div class="x_content">
                                        <p></p>
                                        <div class="form-group">
                                            <label for="">Feature image</label>
                                            <input type="file">
                                        </div>
                                        <div class="form-group">
                                            <p></p>
                                            <button type="submit" class="btn btn-success">submit</button>
                                            <button type="submit" class="btn btn-danger">delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
    
@endsection