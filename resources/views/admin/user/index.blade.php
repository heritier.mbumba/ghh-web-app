@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="d-flex justify-content-between">
                        <h2>Admin Users</h2>
                        <div class="actions">
                            <a href="{{ route('adminuser.create') }}"><span class="icon-user-add"></span></a>
                            
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                </div>
            </div>
            <table class="table table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Country</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td><a href="{{ route('adminuser.edit', $user->id) }}">{{ $user->name }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td>
                                @isset($user->country->name)
                                {{$user->country->name}}
                                @endisset
                            </td>
                            <td class="d-flex align-items-center">
                                <a href="{{ route('adminuser.edit', $user->id) }}" class="btn pb-0 pt-0"><span class="text-success icon-edit-1"></span></a>
                                <form action="{{ route('adminuser.destroy', $user->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class=""><span class="icon-trash-empty"></span></button>
                                  </form>
                            </td>
                            
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
@endsection