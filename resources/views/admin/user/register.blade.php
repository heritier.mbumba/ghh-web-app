@extends('layouts.admin')

@section('content')

  <div class="row">
      <div class="col-md-12">
          <div class="x_panel">
              <div class="x_title">
                <h2>Register new user</h2>
                <div class="clearfix"></div>
              </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-12">
        <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
          <form action="{{ route('adminuser.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-9">
                  <div class="x_panel">
                      <div class="x_content">
                          <div class="form-group">
                            <label for="">First name</label>
                            <input type="text" class="form-control" name="name">
                          </div>
                          <div class="form-group">
                            <label for="">Last name</label>
                            <input type="text" class="form-control" name="lastname">
                          </div>
                          <div class="form-group">
                            <label for="">Email address</label>
                            <input type="email" class="form-control" name="email">
                          </div>
                          <div class="form-group">
                            <label for="">Phone</label>
                            <input type="telephone" class="form-control" name="phone">
                          </div>
                          <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                          </div>

                         
                      </div>
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Options</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <div class="form-group">
                            <label for="">Country</label>
                            <select name="country_id" id="" class="form-control">
                                @foreach ($countries as $country)
                                  <option value="{{ $country->id }}">{{$country->name}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Role</label>
                            <select name="role_id" id="" class="form-control">
                                @foreach ($roles as $role)
                                  <option value="{{ $role->id }}">{{$role->name}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="user__profile">
                              <img src="{{ asset('images/user-placeholder.png')}}" alt="">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
          </form>
      </div>
  </div>
@endsection