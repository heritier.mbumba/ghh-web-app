@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Edit User</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                
            </div>
            
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <form action="{{ route('adminuser.update', $user->id) }}" method="POST" id="user-register-form" data-parsley-validate class="form-horizontal form-label-left">
          @csrf
          @method('PUT')
          <div class="row">
            <div class="col-md-9">
              <div class="x_panel">
                <div class="x_content">
                  <div class="form-group">
                    <label for="Name">First name</label>
                    <input type="text" id="name" required="required" class="form-control " name="name" value="{{ $user->name }}">
                  </div>
                  <div class="form-group">
                    <label for="">Last name</label>
                    <input type="text" id="lastname" name="lastname" class="form-control" value="{{$user->lastname}}">
                  </div>
                  <div class="form-group">
                    <label for="">Email address</label>
                    <input id="email-address" class="form-control" type="text" name="email" value="{{ $user->email }}">
                  </div>
                  <div class="form-group">
                    <label for="">Phone</label>
                    <input id="phone" class="form-control" type="text" name="phone" value="{{$user->phone}}">
                  </div>
                  
                  <button type="submit" class="btn btn-success">Update</button>
                  
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="x_panel">
                <div class="x_content">
                    @if (Auth::user()->role->name != 'administrator')
                      <div class="form-group">
                        <label for="">Country</label>
                        <select name="country_id" id="" class="form-control">
                          @foreach ($countries as $country)
                              <option value="{{ $country->id }}" {{ $user->country->name == $country->name ? "selected" : '' }}>{{ $country->name }}</option>
                          @endforeach
                        </select>
                      </div>
                    @endif
                  <div class="user__profile">
                    <img src="{{asset('images/image-placeholder.svg')}}" alt="">
                    <h4>{{$user->name}}</h4>
                    <select name="role_id" id="">
                      <p>{{$user->role->name}}</p>
                      @foreach ($roles as $role)
                      <option value="{{ $role->id }}" {{$user->role->name === $role->name ? "selected" : ""}}>{{ $role->name }}</option>
                      @endforeach
                    </select>
                    <a href="mailto:">{{$user->email}}</a>
                    <p>
                      @if ($user->country)
                      <span>{{$user->country->name}}</span>
                          
                      @endif
                    </p>
                    <a href="" id="reset-password" class="text-danger">Reset password</a>
                  </div>
                  
                  
                </div>
              </div>
            </div>
          </div>
        </form>
        <form action="{{ route('adminuser.destroy', $user->id) }}" method="post">
          @csrf
          @method('DELETE')
          <button type="submit" class="btn text-danger">Delete</button>
        </form>
      </div>
    </div>
@endsection