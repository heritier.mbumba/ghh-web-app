@extends('layouts.admin')

@section('content')
    
      <div class="row">
        <div class="col-md-4 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Add new role</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form method="POST" action="{{ route('role.add') }}">
                @csrf
                <div class="form-group">
                  <label class="col-form-label" for="last-name">Role Name <span class="required">*</span>
                  </label>
                  <input type="text" id="name" name="name" required="required" class="form-control">
                  
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6">
                      <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
          
        </div>
        <div class="col-md-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Roles</h2>
                
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>   
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $role)
                            <tr>
                                <td class="d-flex align-items-center justify-content-between">
                                    <span>{{ucfirst($role->name)}}</span>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <button type="button" class="btn"><span class="icon-edit"></span></button>
                                        <form action="" method="post">
                                            @csrf
                                            <button type="submit" class="btn"><span class="icon-trash-empty"></span></button>
                                        </form>
                                    </div>
                                </td>
                                
                                
                            </tr>
                                
                            @endforeach
                        

                        
                        </tbody>
                    </table>
                </div>
        </div>
        </div>
      </div>
    
   
@endsection