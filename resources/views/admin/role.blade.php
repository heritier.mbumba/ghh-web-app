@extends('layouts.admin')

@section('content')
    
      <div class="row">
        <div class="col-md-4 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Add role</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form method="POST" action="{{ route('adminrole.store') }}">
                @csrf
                <div class="form-group">
                  <label class="col-form-label" for="last-name">Name <span class="required">*</span>
                  </label>
                  <input type="text" id="name" name="name" required="required" class="form-control">
                
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>

              </form>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="x_panel">
            <div class="x_title">
              <h2>Languages</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Actions</th>
                    
                  </tr>
                </thead>
                <tbody>

                    @foreach ($roles as $role)

                    <tr>
                        
                        <td>{{$role->name}}</td>
                        <td class="d-flex align-items-center">
                            <a href="" class="btn"><span class="icon-edit-1"></span></a>
                            <form action="" method="post">
                                @csrf
                                <input type="hidden" value="{{ $role->id }}">
                                <button type="submit" class="btn"><span class="icon-trash-empty"></span></button>
                            </form>
                        </td>
                        
                      </tr>
                        
                    @endforeach  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    
   
@endsection