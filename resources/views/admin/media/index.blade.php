@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Media</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="actions">
                    <form action="{{ route('adminmedia.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="image" id="">
                        <button class="btn btn-primary">Upload</button>
                    </form>
                 </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                   
                    <div class="file-uploader-wrapper d-flex justify-content-start flex-wrap">
            
                        <div class="file-item" data-image="{{ asset('media/placeholder.jpg') }}">
                            <span id="dele-media" class="icon-trash-empty"></span>
                        </div>
                        <div class="file-item" data-image="{{ asset('media/placeholder.jpg') }}">
                            <span id="dele-media" class="icon-trash-empty"></span>
                        </div>
                        <div class="file-item" data-image="{{ asset('media/placeholder.jpg') }}">
                            <span id="dele-media" class="icon-trash-empty"></span>
                        </div>
                        <div class="file-item" data-image="{{ asset('media/placeholder.jpg') }}">
                            <span id="dele-media" class="icon-trash-empty"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection