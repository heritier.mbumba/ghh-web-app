@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Add new category</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form method="POST" action="{{ route('admincategory.store') }}">
                    @csrf
                    <div class="form-group">
                      <label class="col-form-label" for="last-name">Category Name <span class="required">*</span>
                      </label>
                      <input type="text" id="name" name="name" required="required" class="form-control">
                      
                    </div>
                    <div class="form-group">
                        <label for="">Group</label>
                        <select name="group_id" id="" class="form-control">
                            <option value="">select group</option>
                            @foreach ($groups as $group)
                                <option value="{{ $group->id }}">{{$group->name}}</option>
                            @endforeach
                        </select>
                        <br />
                        <span id="addgroup">add new group</span>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                      <div class="col-md-6 col-sm-6">
                          <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
    
                  </form>
                </div>
              </div>
        </div>
        <div class="col-md-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Categories</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td>
                                        <span>{{ ucfirst($category->name) }}</span>    
                                    </td>
                                    <td>
                                        @if ($category->group)
                                            <span>{{ $category->group->name }}</span>
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection