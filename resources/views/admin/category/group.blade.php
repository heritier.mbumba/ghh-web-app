@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Add new category group</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form method="POST" action="{{ route('admingroup.store') }}">
                    @csrf
                    <div class="form-group">
                      <label class="col-form-label" for="last-name">Group Name <span class="required">*</span>
                      </label>
                      <input type="text" id="name" name="name" required="required" class="form-control">
                      
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                      <div class="col-md-6 col-sm-6">
                          <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
    
                  </form>
                </div>
              </div>
        </div>
        <div class="col-md-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Groups</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($groups as $group)
                                <tr>
                                    <td class="d-flex align-items-center justify-content-between">
                                        <span>{{ ucfirst($group->name) }}</span>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <button type="button" class="btn" data-category-id="{{ $group->id }}" data-category-name="{{$group->name}}"><span class="icon-edit"></span></button>
                                            <form action="{{ route('admincategory.destroy', $group->id) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn"><span class="icon-trash-empty"></span></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection