<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- preload fonts START--}}
    <link href="{{asset('fontello/font/fontello.eot')}}" onload="this.as='font'; this.rel='preload'">
    <link type="font/ttf" href="{{asset('fontello/font/fontello.ttf')}}"  onload="this.as='font'; this.rel='preload'">
    <link type="font/woff" href="{{asset('fontello/font/fontello.woff')}}"  onload="this.as='font'; this.rel='preload'">
    <link type="font/woff2" href="{{asset('fontello/font/fontello.woff2')}}"  onload="this.as='font'; this.rel='preload'">
    {{-- preload fonts END --}}
    <link rel="stylesheet" href="{{asset('fontello/css/fontello-codes.css')}}">
    <link rel="stylesheet" href="{{asset('fontello/css/animation.css')}}">
    <link rel="stylesheet" href="{{asset('fontello/css/fontello-embedded.css')}}">
    <link rel="stylesheet" href="{{asset('fontello/css/fontello-ie7-codes.css')}}">
    <link rel="stylesheet" href="{{asset('fontello/css/fontello-ie7.css')}}">
    <link rel="stylesheet" href="{{asset('fontello/css/fontello.css')}}">

    {{-- OWL CAROUSEL START --}}
    <link rel="stylesheet" href="{{asset('owl-carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('owl-carousel/assets/owl.theme.default.min.css')}}">
    {{-- OWL CAROUSEL END --}}

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    
    {{-- carousels-with-flickity --}}
    <link rel="stylesheet" href="{{asset('css/flickity.css')}}">
    
    
    @if (isset($title))
        <title>{{ $title }} - GHH</title>
    @else
        <title>Home - Generation Hip Hop Global</title>
    @endif
    
</head>
<body>
    
    <main id="qki-main-page">
        <div class="qki-page">
            
            @section('sidebar')
                @include('includes.header')
            @show

            <div class="page__content">
                @yield('content')
            </div>
            
            @include('includes.footer-2')
            
            @include('includes.mega-menu')
        </div>

    </main>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/flickity.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/waltzer-carousel/waltzerjs.min.js') }}"></script>
    <script src="{{ asset('js/ghh-custom.js') }}"></script>

    <script>
        var el = document.getElementById('footer_date')
        var currentYear = new Date().getFullYear()
        el.innerText = currentYear
    </script>
</body>
</html>