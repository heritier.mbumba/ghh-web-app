<section class="page__header d-flex align-items-end" data-bg-img="{{$bgimg}}" data-bg-color>
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div class="page__header_content coursel__item_content">
                    <h1 class="display-2">{{ $title }}</h1>
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</section>