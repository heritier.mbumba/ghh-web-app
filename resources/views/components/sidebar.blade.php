<div class="sidebar__container">
    <h2>{{ $title }}</h2>
    <ul class="sidebar__content">
        {{ $slot }}
    </ul>
</div>