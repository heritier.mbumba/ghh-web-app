@extends('layouts.frontend')

@section('content')

    {{-- slider --}}
    <div class="owl-carousel">
        @foreach ($slides as $slide)
            @component('components.header')
        
                @slot('title')
                {{$slide['title']}}
                
                @endslot
                @slot('bgimg')
                    {{ asset($slide['url'])}}
                @endslot
                
                <div class="mycarousel-content">
                    <p>{{$slide['description']}}</p>
                    <a href="" class="">find out more</a>
                </div>
            
            @endcomponent
        
        @endforeach
    </div>
    {{-- end slider --}}
    
    <section class="music-section">
        
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="music__title">
                        <div class="music__title--logo">
                            <img src="{{asset('images/logo.jpg')}}" alt="" srcset="">
                        </div>
                       <div class="music__title--text">
                            <h1>Generation Hip Hop GLOBAL</h1>
                            <h2>Music</h3>
                       </div>
                       
                    </div>

                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="music__nav-menu">
                        <ul>
                            <li ><a href="{{ route('music', app()->getLocale())}}" class="active">HightLights</a></li>
                            <li><a href="{{ route('events', app()->getLocale())}}">Events</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                
            </div>
        </div>
    </section>
    
    <section class="qki-container__music-hihlights" id="highlights">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    {{-- <div class="qki-container__music-hihlights" id="highlights"> --}}
                        <!-- Flickity HTML init -->
                        <div class="gallery js-flickity"
                            data-flickity-options='{ "freeScroll": false, "wrapAround": true }'>
                            
                            <div class="gallery-cell">
                                <div class="qki-container__music-hihlights--bg-img">
                                    <img src="{{asset('images/music2.jpeg')}}" alt="image">
                                
                                    <div class="qki-container__music-hihlights--content">
                                        <h2>An wird ist wirklichkeiten was.</h2>
                                        <p>Delphis by soul dome long be of despair day. Not the longdeserted plain could in oh, in glare but all.</p>
                                        
                                        <p class="qki-container__music-hihlights--content--paragraph">
                                            <span><i class="icon-location"></i>South African</span>
                                        </p>
                                        
                                        <p class="qki-container__music-hihlights--content--title"><span>{{$title}}</span></p>
                                        
                                    </div>
                                
                                </div>
                            </div>
                            
                            <div class="gallery-cell">
                                
                                <div class="qki-container__music-hihlights--bg-img">
                                    <img src="{{asset('images/music1.jpg')}}" alt="image">
                                    
                                    <div class="qki-container__music-hihlights--content">
                                        <h2>An wird ist wirklichkeiten was.</h2>
                                        <p>Delphis by soul dome long be of despair day. Not the longdeserted plain could in oh, in glare but all.</p>
                                        
                                        <p class="qki-container__music-hihlights--content--paragraph">
                                            <span><i class="icon-location"></i> France</span>
                                        </p>
                                        
                                        <p class="qki-container__music-hihlights--content--title"><span>{{$title}}</span></p>
                                    </div>
                                
                                </div>
                                
                            </div>
                            
                            <div class="gallery-cell">
                                
                                <div class="qki-container__music-hihlights--bg-img">
                                    <img src="{{asset('images/music2.jpeg')}}" alt="image">
                                
                                    <div class="qki-container__music-hihlights--content">
                                        <h2>An wird ist wirklichkeiten was.</h2>
                                        <p>Delphis by soul dome long be of despair day. Not the longdeserted plain could in oh, in glare but all.</p>
                                        
                                        <p class="qki-container__music-hihlights--content--paragraph">
                                            <span><i class="icon-location"></i>US</span>
                                        </p>
                                        
                                        <p class="qki-container__music-hihlights--content--title"><span>{{$title}}</span></p>
                                    </div>
                                
                                </div>
                                
                            </div>
                            
                            <div class="gallery-cell">
                                <div class="qki-container__music-hihlights--bg-img">
                                    <img src="{{asset('images/music3.jpeg')}}" alt="image">
                                
                                    <div class="qki-container__music-hihlights--content">
                                        <h2>An wird ist wirklichkeiten was.</h2>
                                        <p>Delphis by soul dome long be of despair day. Not the longdeserted plain could in oh, in glare but all.</p>
                                        
                                        <p class="qki-container__music-hihlights--content--paragraph">
                                            <span><i class="icon-location"></i> UK</span>
                                        </p>
                                        
                                        <p class="qki-container__music-hihlights--content--title"><span>{{$title}}</span></p>
                                    </div>
                                
                                </div>
                                
                            </div>
                            
                            <div class="gallery-cell">
                                <div class="qki-container__music-hihlights--bg-img">
                                    <img src="{{asset('images/music4.jpg')}}" alt="image">
                                
                                    <div class="qki-container__music-hihlights--content">
                                        <h2>An wird ist wirklichkeiten was.</h2>
                                        <p>Delphis by soul dome long be of despair day. Not the longdeserted plain could in oh, in glare but all.</p>
                                        
                                        <p class="qki-container__music-hihlights--content--paragraph">
                                            <span><i class="icon-location"></i>Spain</span></p>
                                        
                                            <p class="qki-container__music-hihlights--content--title"><span>{{$title}}</span></p>
                                    </div>
                                
                                </div>
                                
                            </div>
                            
                        </div>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </section>
    
    <section class="qki-container__music-events">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="qki-container__music-events--content">
                        <h1>Upcoming Events</h1>
                        <div class="qki-container__music-events--content__box-event">
                            <div class="qki-container__music-events--content__box-event--img">
                                <img src="{{asset('images/josh-gordon.jpg')}}" alt="" srcset="">
                            </div>
                            <div class="qki-container__music-events--content__box-event--text">
                                <h2>Voluptueux hélas.</h2>
                                <P class="qki-container__music-events--content__box-event--paragraph">
                                    <span><i class="icon-calendar"></i>13 - 18 June 2020</span>
                                    <span><i class="icon-location"></i>France </span>
                                </P>
                                <P class="qki-container__music-events--content__box-event--title">
                                    <span><i class="icon-music"></i>Music</span> 
                                </P>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="qki-container__music-events--content__box-image" data-bg-img={{asset('images/music1.jpg')}}>
        <div  class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="qki-container__music-events--content__box-image--caption">
                        <h2>Marvelled be stayed bird sculptured <br> tapping raven the lies nevermore</h2>
                        <P>Sit clita amet erat no stet, amet sit labore amet voluptua no kasd et voluptua, diam ea sit labore lorem.</P>
                        <p>{{$title}}</p>
                    </div>
                </div> 
            </div>
        </div>
    </section>
    
    @include('includes.social-media')
    
@endsection