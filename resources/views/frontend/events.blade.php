@extends('layouts.frontend')

@section('content')

    {{-- slider --}}
    <div class="owl-carousel">
        @foreach ($slides as $slide)
            @component('components.header')
        
                @slot('title')
                {{$slide['title']}}
                
                @endslot
                @slot('bgimg')
                    {{ asset($slide['url'])}}
                @endslot
                
                <div class="mycarousel-content">
                    <p>{{$slide['description']}}</p>
                    <a href="" class="">find out more</a>
                </div>
            
            @endcomponent
        
        @endforeach
    </div>
    {{-- end slider --}}
    
    {{-- start events section --}}
    <section class="events">
        <div class="container">
            {{-- start title --}}
            <div class="row u-margin-top-medium u-margin-bottom-medium">
                <div class="col-md-12 ">
                    <div class="events__title">
                        <h1>All events</h1>
                    </div>
                </div>
            </div>
            {{-- end title --}}
            
            {{-- start content page events --}}
            <div class="row ">  
                {{--start events-select-box --}}
                <div class="events__filter-list u-margin-bottom-big" id="filter-list">
                    
                    <div class="col-md-3 events__select-custom">
                        
                        <div class="events__selected" id="selectCountry">
                            Select country <i class="icon-down-open-1"></i>
                        </div>
                        
                        <div class="events__select-box country">
                            
                            <div class="events__options ">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="france" name="country">
                                    <label for="france">France</label>
                                </div>
                            </div>
                            
                            <div class="events__options">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="us" name="country">
                                    <label for="us">US</label>
                                </div>
                            </div>
                            
                            <div class="events__options">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="za" name="country">
                                    <label for="za">South Africa</label>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-3 select-custom">
                        <div class="events__selected" id="selectClub">
                            Select club<i class="icon-down-open-1"></i>
                        </div>
                        
                        <div class="events__select-box club">
                            
                            <div class="events__options">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="france" name="country">
                                    <label for="france">club name</label>
                                </div>
                            </div>
                            
                            <div class="events__options">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="us" name="country">
                                    <label for="us">club name</label>
                                </div>
                            </div>
                            
                            <div class="events__options">
                                <div class="events__options--option">
                                    <input type="radio" class="radio" id="za" name="country">
                                    <label for="za">club name</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <ul class="events__select-menu">
                            <li class="events__select-menu--item upcoming active"><a href="#">Upcoming</a></li>
                            <li class="events__select-menu--item past"><a href="#">Past</a></li>
                        </ul>
                    </div>
                    
                    <div class="col-md-3">
                        <ul class="events__select-menu">
                            <li class="events__select-menu--item all  active"><a href="#">All</a></li>
                            <li class="events__select-menu--item live "><a href="#">Live</a></li>
                        </ul>
                    </div>
                    
                </div>
                {{-- end events-select-box --}}
    
                {{-- Display content event filtered on the select-box above--}}
                <div class="col-md-12" id="display_content_filter-list">
                    
                    <div class="row u-margin-bottom-small event-box">
                        <div class="event">
                            <figure class="event__shape">
                                <img src="{{asset('images/event2.jpg')}}" alt="Person on a tour" class="event__img">
                                <figcaption class="event__caption">
                                    <span> 21 </span> <br>
                                    <span>February</span> 
                                </figcaption> 
                            </figure>
                            <div class="event__text">
                                <h3 class="heading-tertiary u-margin-bottom-small">Club's name</h3>
                                <h3 class="heading-tertiary u-margin-bottom-small">I had the best week ever with my family</h3>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur libero repellat quis consequatur
                                    ducimus quam nisi exercitationem omnis earum qui. Aperiam, ipsum sapiente aspernatur libero
                                    repellat quis consequatur ducimus quam nisi exercitationem omnis earum qui.<br> 
                                    <span class="paragraph__icon"><i class="icon-location"></i> France</span>
                                </p>
                            </div>
                        </div>
                    </div>
        
                    <div class="row u-margin-bottom-small event-box">
                        <div class="event">
                            <figure class="event__shape">
                                <img src="{{asset('images/event1.jpg')}}" alt="Event image" class="event__img">
                                <figcaption class="event__caption">
                                    <span>01 - 21</span> <br>
                                    <span>June</span> 
                                </figcaption>
                            </figure>
                            <div class="event__text">
                                <h3 class="heading-tertiary u-margin-bottom-small">Club's name</h3>
                                <h3 class="heading-tertiary u-margin-bottom-small">WOW! My life is completely different now</h3>
                                <p class="paragraph">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur libero repellat quis consequatur
                                    ducimus quam nisi exercitationem omnis earum qui. Aperiam, ipsum sapiente aspernatur libero
                                    repellat quis consequatur ducimus quam nisi exercitationem omnis earum qui.<br> 
                                    <span class="paragraph__icon"><i class="icon-location"></i> France</span>
                                </p>
    
                            </div>
                        </div>
                    </div>
        
                    <div class="u-center-text u-margin-top-small u-margin-bottom-medium ">
                        <a href="#" class="btn__text events--link">Load More</a>
                    </div>
                </div>
    
            </div>
            {{-- ends content page events --}}
            
        </div>
    </section>
    
    {{-- end section --}}
    
    
@endsection