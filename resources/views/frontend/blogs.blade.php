@extends('layouts.frontend')

@section('content')
    @component('components.header')
    @slot('bgimg')
        {{asset('images/bg-img-1.jpg')}}
    @endslot
        @slot('title')
            Blogs
        @endslot
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, debitis?</p>
    @endcomponent

    <section class="blog__page">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @component('components.sidebar')
                        @slot('title')
                            Blogs category
                        @endslot
                        <li><a href="">Music</a></li>
                        <li><a href="">Dance</a></li>
                        <li><a href="">Video</a></li>
                        <li><a href="">Sport</a></li>
                    @endcomponent
                </div>
                <div class="col-md-9">
                    <div class="blog__list_container d-flex align-items-center justify-content-between">
                        @component('components.card-item')
                                <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                                <div class="card__content">
                                    <h4>Blog 1</h4>
                                    <span class="meta__info">meta description</span>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet, sequi!...</p>
                                    <a href="" class="btn btn-primary">read more</a>
                                </div>
                        @endcomponent
                        @component('components.card-item')
                                <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                                <div class="card__content">
                                    <h4>Blog 1</h4>
                                    <span class="meta__info">meta description</span>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet, sequi!...</p>
                                    <a href="" class="btn btn-primary">read more</a>
                                </div>
                        @endcomponent
                        @component('components.card-item')
                                <img src="{{ asset('images/image-placeholder.svg') }}" alt="">
                                <div class="card__content">
                                    <h4>Blog 1</h4>
                                    <span class="meta__info">meta description</span>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet, sequi!...</p>
                                    <a href="" class="btn btn-primary">read more</a>
                                </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection