<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->firstName,
        'email'=>$faker->unique()->email,
        'email_verified_at' => now(),
        'role_id'=>2,
        'country_id'=>$faker->numberBetween(1, 61),
        'password'=>'$2y$10$yTckdztztFTDAN1vf4HmNuTBXtx.fRu2vOSPjxk9ICr0ytQHzCjAy',
        'lastname'=>$faker->lastName,
        'phone'=>$faker->e164PhoneNumber
    ];
});
