<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        //
        'title'=>$faker->realText(50, 2),
        'description'=>$faker->paragraphs(4, true),
        'slug'=>$faker->slug,
        'admin_id'=>$faker->numberBetween(1, 61),
        'country_id'=>$faker->numberBetween(1, 61),
        'category_id'=>$faker->numberBetween(1, 20)
    ];
});
