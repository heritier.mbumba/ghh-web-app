<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Country;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->country,
        'slug'=>$faker->slug,
        'language_id'=>$faker->numberBetween(1, 46)

    ];
});
