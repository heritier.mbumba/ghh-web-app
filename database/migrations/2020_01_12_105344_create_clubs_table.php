<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('clubs')){
            return;
        }
        
        Schema::create('clubs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('address');
            $table->integer('admin_id');
            $table->bigInteger('country_id')->unsigned();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            
            $table->timestamps();
            
            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();

        Schema::dropIfExists('clubs');
Schema::disableForeignKeyConstraints();
    }
}
