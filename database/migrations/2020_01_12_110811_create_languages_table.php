<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('languages')){
            return;
        }
        
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prefix');
            $table->string('name')->unique();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
