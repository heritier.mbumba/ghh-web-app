<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('projects')){
            return;
        }
        
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('description');
            $table->string('slug')->unique();
            $table->integer('admin_id');
            $table->integer('tag_id');
            $table->integer('category_id');
            $table->bigInteger('country_id')->unsigned();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            $table->integer('projects_media_id')->default(1);
            $table->timestamps();

            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::enableForeignKeyConstraints();

        Schema::dropIfExists('projects');
Schema::disableForeignKeyConstraints();
    }
}
