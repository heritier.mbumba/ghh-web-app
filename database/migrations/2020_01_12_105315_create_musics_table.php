<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('musics')){
            return;
        }
        
        
        Schema::create('musics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('description');
            $table->string('artist_name');
            $table->integer('media_id');
            $table->text('url')->nullable();
            $table->integer('admin_id');
            $table->bigInteger('country_id')->unsigned();
            $table->integer('club_id')->nullable();
            $table->integer('tag_id');
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            $table->timestamps();
            
            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('musics');

    }
}
