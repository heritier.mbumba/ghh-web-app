<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('blogs')){
            return;
        }

        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 100);
            $table->longText('description');
            $table->string('slug')->unique();
            $table->integer('category_id');
            $table->integer('tag_id')->nullable();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            $table->integer('admin_id');
            $table->bigInteger('country_id')->unsigned();
            $table->integer('feature_image_id')->default(1);
            $table->integer('club_id')->nullable();
            $table->timestamps();
            
            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('blogs');


    }
}
