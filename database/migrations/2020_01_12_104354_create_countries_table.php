<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('countries')){
            return;
        }
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('language_id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('flag_url_id')->nullable();
            $table->integer('feature_image_id')->nullable();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            $table->timestamps();

        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
