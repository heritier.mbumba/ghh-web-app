<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('products')){
            return;
        }
        
        
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
            $table->decimal('price', 8, 2);
            $table->string('slug')->unique();
            $table->integer('tag_id');
            $table->integer('category_id');
            $table->integer('media_id');
            $table->integer('admin_id');
            $table->bigInteger('country_id')->unsigned();
            $table->enum('status', ['published', 'unpublished', 'trashed', 'draft'])->default('unpublished');
            $table->timestamps();
            
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('products');

    }
}
